package com.Bank;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class CalculateDollarsServlet
 */
@WebServlet("/CalculateDollarsServlet")
public class CalculateDollarsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CalculateDollarsServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}
	
	private Integer doubledInputNumber(final String number) {
		if(number == null || number.length() == 0 || number.matches("[0-9]+") == false) return 0;
		return Integer.parseInt(number) * 2;
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
		request.setAttribute("dollarsInputNumber", doubledInputNumber(request.getParameter("dollarsInputNumber")));
		getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
	}
}
